package com.thource.dev.be_quiet.listeners;

import com.thource.dev.be_quiet.BeQuiet;
import org.spongepowered.api.effect.sound.SoundTypes;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.Order;
import org.spongepowered.api.event.sound.PlaySoundEvent;

import java.util.Arrays;
import java.util.List;

public class SoundListener {
    private final List SOUND_BLACKLIST = Arrays.asList(
            SoundTypes.ENTITY_WITHER_SPAWN,
            SoundTypes.ENTITY_ENDERDRAGON_DEATH
    );

    @Listener(order = Order.FIRST, beforeModifications = true)
    public void onPlaySoundBroadcast(PlaySoundEvent.Broadcast event) {
        BeQuiet.getLogger().debug("onPlaySoundBroadcast - " + event.getSoundType().getId());

        if (SOUND_BLACKLIST.contains(event.getSoundType())) event.setCancelled(true);
    }
}
