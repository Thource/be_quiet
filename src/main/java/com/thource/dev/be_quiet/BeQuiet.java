package com.thource.dev.be_quiet;

import com.google.inject.Inject;
import com.thource.dev.be_quiet.listeners.SoundListener;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "be_quiet",
        name = "Be_quiet",
        authors = {
                "Thource"
        }
)
public class BeQuiet {

    private static BeQuiet plugin;

    @Inject
    private Logger logger;

    @Listener
    public void onInit(GameInitializationEvent event)
    {
        plugin = this;
    }

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
        logger.info("Initializing");

        Sponge.getEventManager().registerListeners(this, new SoundListener());

        logger.info("Initialized");
    }

    public static BeQuiet getInstance()
    {
        return plugin;
    }

    public static Logger getLogger()
    {
        return getInstance().logger;
    }
}
